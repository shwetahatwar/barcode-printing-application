package com.example.keystoreapplicationforsignedapk

import android.content.Context
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.print.PrintAttributes
import android.print.PrintManager
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemServiceName
import com.itextpdf.text.*
import com.itextpdf.text.pdf.BaseFont
import com.itextpdf.text.pdf.PdfWriter
import com.itextpdf.text.pdf.draw.LineSeparator
import com.itextpdf.text.pdf.draw.VerticalPositionMark
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import kotlinx.android.synthetic.main.printing_barcode_fragment.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.PrintWriter
import java.net.Socket
import androidx.core.content.ContextCompat.getSystemService as getSystemService1

class PrintingBarcodeFragment : Fragment() {

    companion object {
        fun newInstance() = PrintingBarcodeFragment()
    }

    private lateinit var viewModel: PrintingBarcodeViewModel
    val file_name: String = "test_pdf.pdf"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.printing_barcode_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(PrintingBarcodeViewModel::class.java)


        print_button.setOnClickListener {
            createPDFFile(Common.getAppPath(this.requireContext()) + file_name)
            try {
                println("-------------------------------------")
                val sock = Socket("192.168.1.8", 9100)
                val oStream = PrintWriter(sock.getOutputStream())
                oStream.write("HI,test from Android Device")
                //check = "yes"
                Toast.makeText(context,"Inside click",Toast.LENGTH_SHORT).show()

                oStream.flush()
            } catch (e: IOException) {
                e.printStackTrace()
//                Logger.LogError(FragmentActivity.TAG, e.toString())
//                AppToast.showShortToast(mContext, e.toString())
            }
        }


        Dexter.withActivity(this.requireActivity())
            .withPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object: PermissionListener {
                override fun onPermissionGranted(response: PermissionGrantedResponse?) {
                    print_button.setOnClickListener {
                        createPDFFile(Common.getAppPath(requireContext()) + file_name)
                    }
                }

                override fun onPermissionRationaleShouldBeShown(
                    permission: PermissionRequest?,
                    token: PermissionToken?
                ) {
                }

                override fun onPermissionDenied(response: PermissionDeniedResponse?) {
                }

            })
            .check()
    }

    private fun createPDFFile(path: String) {
        if (File(path).exists())
            File(path).delete()
        try {
            val document = Document()

            PdfWriter.getInstance(document, FileOutputStream(path))
            document.open()
            document.pageSize = PageSize.A4
            document.addCreationDate()
            document.addAuthor("Shweta Hatwar")
            document.addCreator("Shweta Chopkar")

            val colorAccent = BaseColor(0,153,204,255)
            val headingFontSize = 20.0f
            val valueFontSize = 26.0f

            val fontName = BaseFont.createFont("assest/fonts/skodaprobold.ttf","UTF-8",BaseFont.EMBEDDED)

            var titleStyle = Font(fontName,36.0f,Font.NORMAL,BaseColor.BLACK)
            addNewItem(document,"Order Details",com.itextpdf.text.Element.ALIGN_CENTER,titleStyle)

            val headingStyle = Font(fontName,headingFontSize,Font.NORMAL,colorAccent)
            addNewItem(document,"Order No.",com.itextpdf.text.Element.ALIGN_LEFT,headingStyle)

            val valueStyle = Font(fontName,valueFontSize,Font.NORMAL,BaseColor.BLACK)
            addNewItem(document,"#123456",com.itextpdf.text.Element.ALIGN_LEFT,valueStyle)

            addLineSeprator(document)

            addNewItem(document,"Order Date.",com.itextpdf.text.Element.ALIGN_LEFT,headingStyle)
            addNewItem(document,"30/10/2020",com.itextpdf.text.Element.ALIGN_LEFT,valueStyle)

            addLineSeprator(document)

            addNewItem(document,"Account Name.",com.itextpdf.text.Element.ALIGN_LEFT,headingStyle)
            addNewItem(document,"BRiOT Technologies Pvt Ltd",com.itextpdf.text.Element.ALIGN_LEFT,valueStyle)

            addLineSeprator(document)

            //
            addLineSpace(document)
            addNewItem(document,"Product Details",com.itextpdf.text.Element.ALIGN_CENTER,titleStyle)

            addLineSeprator(document)

            addNewItemWithLeftAndRight(document,"Pizaa 25","(0.0%)",titleStyle,valueStyle)
            addNewItemWithLeftAndRight(document,"12.0*1000","12000.0",titleStyle,valueStyle)

            addLineSeprator(document)

            addNewItemWithLeftAndRight(document,"Pizaa 26","(0.0%)",titleStyle,valueStyle)
            addNewItemWithLeftAndRight(document,"12.0*1000","12000.0",titleStyle,valueStyle)

            addLineSeprator(document)

            addLineSpace(document)
            addLineSpace(document)

            addNewItemWithLeftAndRight(document,"Total","24000.0",titleStyle,valueStyle)

            document.close()

            Toast.makeText(context,"Success",Toast.LENGTH_SHORT).show()

            //printPDF()

        }catch (e:Exception)
        {
            Log.e("Error",""+e.message)
        }
    }

//    private fun printPDF() {
//        val printManager = getSystemService1(Context.PRINT_SERVICE) as PrintManager
//        try {
//            val printAdapter = PdfDocumentAdapter(this,Common.getAppPath(this)+ file_name)
//            printManager.print("Document", printAdapter, PrintAttributes.Builder().build())
//        }catch (e:Exception){
//            Log.e("Error",""+e.message)
//        }
//    }

    private fun addNewItemWithLeftAndRight(document: Document,
                                           textLeft: String,
                                           textRight: String,
                                           leftStyle: Font,
                                           rightStyle: Font) {

        val chunkTextleft = Chunk(textLeft,leftStyle)
        val chunkTextRight = Chunk(textRight,rightStyle)
        val p = Paragraph(chunkTextleft)
        p.add(Chunk(VerticalPositionMark()))
        p.add(chunkTextRight)
        document.add(p)
    }

    private fun addLineSpace(document: Document) {
        document.add(Paragraph(""))
    }

    private fun addLineSeprator(document: Document) {
        val lineSeparator = LineSeparator()
        lineSeparator.lineColor = BaseColor(0,0,0,68)
        addLineSpace(document)
        document.add(Chunk(lineSeparator))
        addLineSpace(document)

    }

    private fun addNewItem(document: com.itextpdf.text.Document, text: String, align: Int, titleStyle: Font) {
        val chunk = Chunk(text,titleStyle)
        val p = Paragraph(chunk)
        p.alignment = align
        document.add(p)
    }

}