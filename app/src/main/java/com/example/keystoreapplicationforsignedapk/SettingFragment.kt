package com.example.keystoreapplicationforsignedapk

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.Editable
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import com.briot.wms.implementor.repository.local.PrefConstants
import com.briot.wms.implementor.repository.local.PrefRepository
import kotlinx.android.synthetic.main.setting_fragment.*
import java.net.Inet4Address

class SettingFragment : Fragment() {

    companion object {
        fun newInstance() = SettingFragment()
    }

    private lateinit var viewModel: SettingViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.setting_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(SettingViewModel::class.java)
        (this.activity as AppCompatActivity).title = "Setting"

        var old_ip = PrefRepository.singleInstance.getValueOrDefault(PrefConstants().IPADDRESS, "")
        println("ipaddress 4333-->" + PrefRepository.singleInstance.getValueOrDefault(PrefConstants().IPADDRESS, ""))
        fun String.toEditable(): Editable = Editable.Factory.getInstance().newEditable(this)
        setting_textView.text = old_ip.toEditable()

        setting_save_button.setOnClickListener {
            var url = setting_textView.text
            // var url = "https://briot-wms-app.herokuapp.com"
            var ip = ""
            var flag: Boolean = false
            if (url!!.contains("//")) {
                ip = url.split("//")[1]
                if (ip.contains(":")) {
                    ip = ip.split(":")[0]
                    val result = ValidateIPv4.isValidInet4Address(ip)
                    if (result) {
                        // valid ip address
                        flag = true
                        PrefRepository.singleInstance.setKeyValue(PrefConstants().IPADDRESS, url.toString())
                    } else {
                        Toast.makeText(context,"Please enter valid url address", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    flag = true
                    PrefRepository.singleInstance.setKeyValue(PrefConstants().IPADDRESS, url.toString())
                }
            } else {
                Toast.makeText(context,"Please enter valid url address", Toast.LENGTH_SHORT).show()
            }
            if (flag) {
                println("ipaddress-->" + PrefRepository.singleInstance.getValueOrDefault(PrefConstants().IPADDRESS, ""))
                Toast.makeText(context,"submitted successfully", Toast.LENGTH_SHORT).show()
                Navigation.findNavController(it).navigate(R.id.printingBarcodeFragment)
            }
        }
    }

    internal object ValidateIPv4 {
        fun isValidInet4Address(ip: String?): Boolean {
            return try {
                Inet4Address.getByName(ip)
                    .hostAddress.equals(ip)
            } catch (ex: Exception) {
                // Log.d("Exception in ip ", ex.getLocalizedMessage());
                false
            }
        }
    }
}